# README #

Building a simple Nginx image docker and push up hub.docker.com

# 1. Building image

In directory which contain Dockerfile:

docker build -t yourname/nginx_basic .

Test:

docker run -p 9000:80 -d yourname/nginx_basic

http://localhost:9000

# 2. Push image up hub.docker.com

Login hub.docker.com:

docker login

Push image up hub:

docker push yourname/nginx_basic

